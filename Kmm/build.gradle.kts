plugins {
    kotlin("multiplatform") version "1.9.20"
    id("org.openjfx.javafxplugin") version "0.1.0"
}

group = "de.musoft.stereoscope"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    jvm()
    js {
        browser()
    }

    sourceSets {
        commonTest {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        commonMain {
            dependencies {
            }
        }
        jvmMain {
            dependencies {
                // As JavaFX have platform-specific dependencies, we need to add them manually
                val fxSuffix = when (osdetector.classifier) {
                    "linux-x86_64" -> "linux"
                    "linux-aarch_64" -> "linux-aarch64"
                    "windows-x86_64" -> "win"
                    "osx-x86_64" -> "mac"
                    "osx-aarch_64" -> "mac-aarch64"
                    else -> throw IllegalStateException("Unknown OS: ${osdetector.classifier}")
                }

                implementation("org.openjfx:javafx-base:18.0.2:${fxSuffix}")
                implementation("org.openjfx:javafx-graphics:18.0.2:${fxSuffix}")
                implementation("org.openjfx:javafx-controls:18.0.2:${fxSuffix}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.7.3")
            }
        }
        jsMain {
            dependencies {
            }
        }
    }
}

tasks.withType<Wrapper> {
    gradleVersion = "8.1.1"
    distributionType = Wrapper.DistributionType.BIN
}