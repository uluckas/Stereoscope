pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
}

include(":Kmm")
include(":FxApp")
include(":BrowserApp")
