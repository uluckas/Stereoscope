plugins {
    kotlin("multiplatform") version "1.9.20"
}

group = "de.musoft.stereoscope"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    js {
        browser()
        binaries.executable()
    }

    sourceSets {
        commonTest {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        jsMain {
            dependencies {
                implementation(project(":Kmm"))
            }
        }
    }
}
