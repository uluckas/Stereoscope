plugins {
    kotlin("jvm") version "1.9.20"
    application
    id("org.openjfx.javafxplugin") version "0.1.0"
}

group = "de.musoft.stereoscope"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":Kmm"))

    // As JavaFX have platform-specific dependencies, we need to add them manually
    val fxSuffix = when (osdetector.classifier) {
        "linux-x86_64" -> "linux"
        "linux-aarch_64" -> "linux-aarch64"
        "windows-x86_64" -> "win"
        "osx-x86_64" -> "mac"
        "osx-aarch_64" -> "mac-aarch64"
        else -> throw IllegalStateException("Unknown OS: ${osdetector.classifier}")
    }

    // Replace "compileOnly" with "implementation" for a non-library project
    implementation("org.openjfx:javafx-base:18.0.2:${fxSuffix}")
    implementation("org.openjfx:javafx-graphics:18.0.2:${fxSuffix}")
    implementation("org.openjfx:javafx-controls:18.0.2:${fxSuffix}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.7.3")
}
