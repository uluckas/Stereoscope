package de.s2s.stereoscope

import de.s2s.stereoscope.basepattern.RandomBasePattern
import de.s2s.stereoscope.elevation.CircleElevationModel
import de.s2s.stereoscope.platform.FXCanvas
import de.s2s.stereoscope.renderer.Density
import de.s2s.stereoscope.renderer.StereoRenderer
import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.layout.StackPane
import javafx.stage.Screen
import javafx.stage.Stage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.javafx.JavaFx
import kotlinx.coroutines.launch

class Stereoscope : Application() {

    private lateinit var stereoRenderer: StereoRenderer

    private var fxScope: CoroutineScope? = null

    private var shouldExit = false

    override fun start(primaryStage: Stage?) {
        val screen = Screen.getPrimary()
        val dpi = screen.dpi
        val PPMM = (if (dpi < 0.0) 96.0 else dpi) / 25.4 // inch to mm
        val primaryScreenBounds = screen.visualBounds
        val width = primaryScreenBounds.width
        val height = primaryScreenBounds.height

        val canvas = javafx.scene.canvas.Canvas(width, height)
        val root = StackPane(canvas)

        primaryStage?.apply {
            title = "Stereoscope"
            x = primaryScreenBounds.minX
            y = primaryScreenBounds.minY
            setWidth(width)
            setHeight(height)
            scene = Scene(root, width, height)
            setOnCloseRequest {
                Platform.exit()
                shouldExit = true
            }
            show()
        }

        val density = Density(PPMM, PPMM)
        val fxCanvas = FXCanvas(canvas, density)
        stereoRenderer = StereoRenderer(fxCanvas, RandomBasePattern(), CircleElevationModel())

        fxScope?.cancel()
        fxScope = CoroutineScope(kotlinx.coroutines.Dispatchers.JavaFx + kotlinx.coroutines.SupervisorJob())
        fxScope?.launch(start = CoroutineStart.UNDISPATCHED) {
            while (true) {
                stereoRenderer.render()
                delay(20)
            }
        }
    }

    override fun stop() {
        fxScope?.cancel()
        fxScope = null
    }
}

fun main(args: Array<String>) {
    Application.launch(Stereoscope::class.java, *args)
}

